import React from 'react'
import {BsLinkedin, BsInstagram} from 'react-icons/bs'
import {RiGitlabFill} from 'react-icons/ri'

const HeaderSocials = () => {
  return (
    <div className='header__socials'>
        <a href="https://www.linkedin.com/in/paul-chaigneau-6b3956158/" target="_blank" rel="noreferrer"><BsLinkedin/></a>
        <a href="https://gitlab.com/chaigneaupaul" target="_blank" rel="noreferrer"><RiGitlabFill/></a>
        <a href="https://www.instagram.com/etoile_pol_air/" target="_blank" rel="noreferrer"><BsInstagram/></a>
    </div>
  )
}

export default HeaderSocials